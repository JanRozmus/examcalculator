package com.company;

public abstract class ScoreDecorator implements  Score{
    private final Score score;



    public ScoreDecorator(Score score) {
        this.score = score;
    }

/*    public ScoreDecorator() {
        this.score = new Score() {
            @Override
            public int countPoints() {
                return 0;
            }
        };
    }

    public int calculate(ScoreDecorator score){
        return score.countPoints();
    }
    */

    @Override
    public int countPoints() {
        return score.countPoints();
    }
}
