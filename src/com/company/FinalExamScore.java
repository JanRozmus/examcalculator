package com.company;

public class FinalExamScore extends ScoreDecorator {
    private int points;

    public FinalExamScore(Score score, int points) {
        super(score);
        this.points = points;
    }


    public FinalExamScore(Score score) {
        super(score);
    }


    @Override
    public int countPoints() {
        return super.countPoints() + countFinalExamPoints();
    }

    private int countFinalExamPoints() {
        assureScoreRange(points);
        return points;
    }

    private void assureScoreRange(int points) {

        if (points < 0 || points > 100) {
            throw new ValidationException("Wrong score range in FinalExamScore!");
        }
    }
}
