package com.company;

public class PartialExamScore extends ScoreDecorator {
    private final int firstMultiplier;
    private final int secondMultiplier;
    private final int firstPoints;
    private final int secondPoints;

    public PartialExamScore(Score score, int firstMultiplier, int secondMultiplier, int firstPoints, int secondPoints) {
        super(score);
        this.firstMultiplier = firstMultiplier;
        this.secondMultiplier = secondMultiplier;
        this.firstPoints = firstPoints;
        this.secondPoints = secondPoints;
    }

    @Override
    public int countPoints() {
        return super.countPoints() + countPartialExamPoints();
    }

    private void assureScoreRange(int firstScore, int secondScore) {
        if (firstScore < 0 || firstScore > 10 ||secondScore < 0 || secondScore > 10){
            throw new ValidationException("Wrong score range in PartialExamScore!");
        }
    }

    private int countPartialExamPoints() {
        assureScoreRange(firstPoints, secondPoints);
        return firstPoints * firstMultiplier + secondPoints * secondMultiplier;
    }
}
