package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ExamCalculatorTest {
    private ExamCalculator examCalculator;

    @Mock
    public Score score;


    @BeforeEach
    void init() {
        examCalculator = new ExamCalculator();
    }

    @Test
    public void checkIfEverythingIsCalculatingProperly(){
        List<Integer> oceny = new LinkedList<>();
        oceny.add(5);
        oceny.add(2);
        ScoreDecorator score = new FinalExamScore(
                new PartialExamScore(
                        new OptionalGradesScore(
                                new ExamCalculator(), oceny), 1,2,2,2), 20);
        assertEquals(score.countPoints(),33);
    }

    @Test
    public void checkIfWrongRangeInFinalExamScoreThrowsAnError() {

        ScoreDecorator score = new FinalExamScore(new ExamCalculator(),2137);
        assertThrows(ValidationException.class, score::countPoints);
    }

    @Test
    public void checkIfWrongRangeInPartialExamScoreThrowsAnError() {

        ScoreDecorator score = new PartialExamScore(new ExamCalculator(),2, 2, 2, 123);
        assertThrows(ValidationException.class, score::countPoints);
    }
    @Test
    public void checkIfWrongRangeInOptionalGradesScoreThrowsAnError() {
        List<Integer> oceny = new LinkedList<>();
        oceny.add(1);
        oceny.add(2);
        oceny.add(20);
        ScoreDecorator score = new OptionalGradesScore(new ExamCalculator(), oceny);
        assertThrows(ValidationException.class, score::countPoints);
    }
}