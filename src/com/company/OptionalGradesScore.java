package com.company;

import java.util.List;

public class OptionalGradesScore extends ScoreDecorator {
    List<Integer> grades;

    public OptionalGradesScore(Score score, List<Integer> grades) {
        super(score);
        this.grades = grades;
    }

    public OptionalGradesScore(Score score) {
        super(score);
    }

    @Override
    public int countPoints() {
        return super.countPoints() + countOptionalGradesPoints();
    }

    private void assureScoreRange(List<Integer> grades) {
        grades.forEach(s -> {
            if (s > 6 || s < 0)
                throw new ValidationException("Wrong score range in OptionalGradesScore!");
        });
    }

    private int countOptionalGradesPoints() {
        assureScoreRange(grades);
        return grades.stream()
                .reduce(0, Integer::sum);
    }
}
